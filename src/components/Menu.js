import React, { useState } from 'react';
import styled from 'styled-components';

import {  LIGHT_GRAY,  HIGH_BLUE } from '../themes/styles';


export const MenuGroup = styled.ul`
    list-style-type: none;
    font-weight: bold;
    li {
        line-height: 2.5em;
        img {
            vertical-align: middle;
            margin-right: 6px;
        }

        a, a:visited {
            cursor: pointer;
            color: ${LIGHT_GRAY};
            text-decoration: none;
        }

        ul {
            display: none;
        }
    }

    li a.active ~ ul {
        display: block;   
    } 

`;

export const AlternateMenuGroup = styled(MenuGroup)`
    li {
        line-height: 3.6em;
    }
`

export const GroupTitle = ({ thumbnail, label, link, children, className, onClick }) => (
    <li onClick={onClick}>
        <a  href="#" className={className}><img src={thumbnail} alt={label}/> {label}</a>
        {children}
    </li>
)

export const SubGroup = styled.ul`
    font-weight: normal;
    list-style-type: none;
    font-size: 13px;
    li {
        line-height: 2.2em; 
        a:hover {
            color: ${HIGH_BLUE};
        }
    }
    li: last-child {
        margin-bottom: 15px;
    }
`;

export const SubGroupOption = ({ label, onClick}) => {

    return (
        <li onClick={onClick}>
            <a href="#"> {label}</a>
        </li>
    )

}

export const MenuSeparator = styled.hr`
    border-bottom: solid 0.01em ${LIGHT_GRAY};
    opacity: 0.5;
`


export const Menu = ({ menuData = [], groupClick, subClick, MainGroup }) => {
    // good idea to use inmutable js
    const [_menuData, setMenu] = useState(menuData.slice());
    const onGroupClick = (option) => {
        const selectedIndex = _menuData.findIndex(p => p.id === option.id);
        if (selectedIndex) {
            let _menuChanges = menuData.slice();
            if (option.className === 'active') {
                _menuChanges[selectedIndex] = Object.assign({}, _menuChanges[selectedIndex]);
            } else {
                _menuChanges[selectedIndex] = Object.assign({}, _menuChanges[selectedIndex], { className: 'active' });
            } 
            
            setMenu(_menuChanges);
        }
    }

    // can probably simply this methods
    const topClick = (option, event) => {
        event.preventDefault();
        event.stopPropagation();
        onGroupClick(option)
        if (typeof groupClick === 'function') {
             groupClick(option);
        }
    }

    const onSubClick = (option, event) => {
        event.preventDefault();
        event.stopPropagation();
        if (typeof subClick === 'function') {
            subClick(option);
        }
        return false;
    }

    

    const Menu = _menuData.map(group => { 
        const groupTitle = (
            <GroupTitle {...group} onClick={(event) => topClick(group, event)} key={`g-${group.id}`}>
                <SubGroup>{ group.children.map(sub => <SubGroupOption { ...sub } onClick={(event) => onSubClick(sub, event)} key={`l-${sub.id}`}></SubGroupOption>)}</SubGroup>
            </GroupTitle>
        )

        return groupTitle;
    })

    return (
        <MainGroup>
           {Menu}
        </MainGroup>
       
    )
    
}

export const DefaultMenu = (props) => {
    return <Menu {...props} MainGroup={ MenuGroup } />
}

export const AlternateMenu  = (props) => {
    return <Menu {...props} MainGroup={ AlternateMenuGroup } />
}
