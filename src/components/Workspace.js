import React from 'react';
import styled from 'styled-components';
import { CANVAS_BG } from '../themes/styles';

export const Canvas = styled.div`
    background-color: ${CANVAS_BG};
    min-width: 100vw;
    min-height: 100vh;
    padding: 20px;
`