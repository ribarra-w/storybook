// rename to side navigation resources

export const menuResource = {
    en: [
        {
            id: 1,
            label: 'Home',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/home.svg',
            children: []
        },
        {
            id: 2,
            label: 'Monitor your Business',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/monitor.svg',
            link: ' ',
            children:[
                {
                    id: 1,
                    parentId: 2,
                    label: 'Business Overview'
                },
                {
                    id: 2,
                    parentId: 2,
                    label: 'Revenue Summary'
                },
                {
                    id: 3,
                    parentId: 2,
                    label: 'Day of Week Performance'
                },
                {
                    id: 4,
                    parentId: 2,
                    label: 'Card Transactions'
                },
                {
                    id: 5,
                    parentId: 2,
                    label: 'Customer Overview'
                },
                {
                    id: 6,
                    parentId: 2,
                    label: 'Compare your Business'
                },
                {
                    id: 7,
                    parentId: 2,
                    label: 'Compare your Locations'
                },
                {
                    id: 8,
                    parentId: 2,
                    label: 'Flexible Funding Offers'
                }
            ]
        },
        {
            id: 3,
            label: 'Boost your Reputation',
            thumbnail: "https://app00.testing.womply.com/dashboard/assets/icons/boost.svg",
            link: ' ',
            children: [
                {
                    id: 1,
                    parentId: 3, 
                    label: 'Reputation Pages Overview'
                },
                {
                    id: 2,
                    parentId: 3, 
                    label: 'Manage Review'
                },
                {
                    id: 3,
                    parentId: 3, 
                    label: 'Get More Reviews'
                },
                {
                    id: 4,
                    parentId: 3, 
                    label: 'Download the Get Reviews App'
                },
                {
                    id: 5,
                    parentId: 3, 
                    label: 'Reputation Settings'
                }
            ]
        },
        {
            id: 4,
            label: 'Engage your customers',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/engage.svg',
            link: ' ',
            children: [
                {
                    id: 1,
                    parentId: 4, 
                    label: 'Customer Directory'
                },
                {
                    id: 2,
                    parentId: 4,
                    label: 'Offers'
                },
                {
                    id: 3,
                    parentId: 4,
                    label: 'Customer Email Settings'
                },
                {
                    id: 4,
                    parentId: 4,
                    label: 'Customer Email Results'
                },
                {
                    id: 5,
                    parentId: 4,
                    label: 'Manage Customer Feedback'
                } 
            ]
        }
    ],
    es:[
        {
            id: 1,
            label: 'Inicio',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/home.svg',
            children: []
        },
        {
            id: 2,
            label: 'Monitorea tu negocio',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/monitor.svg',
            link: ' ',
            children:[
                {
                    id: 1,
                    parentId: 2,
                    label: 'Visión general del negocio'
                },
                {
                    id: 2,
                    parentId: 2,
                    label: 'Resumen de ingresos'
                },
                {
                    id: 3,
                    parentId: 2,
                    label: 'Rendimiento del día de la semana'
                },
                {
                    id: 4,
                    parentId: 2,
                    label: 'Transacciones con tarjeta'
                },
                {
                    id: 5,
                    parentId: 2,
                    label: 'Resumen del cliente'
                },
                {
                    id: 6,
                    parentId: 2,
                    label: 'Compara tu negocio'
                },
                {
                    id: 7,
                    parentId: 2,
                    label: 'Compara Ubicaciones'
                },
                {
                    id: 8,
                    parentId: 2,
                    label: 'Ofertas de financiación flexibles'
                }
            ]
        },
        {
            id: 3,
            label: 'Aumenta tu reputación',
            thumbnail: "https://app00.testing.womply.com/dashboard/assets/icons/boost.svg",
            link: ' ',
            children: [
                {
                    id: 1,
                    parentId: 3, 
                    label: 'Resumen de páginas de Reputación'
                },
                {
                    id: 2,
                    parentId: 3, 
                    label: 'Administrar revisión'
                },
                {
                    id: 3,
                    parentId: 3, 
                    label: 'Obtenga más reseñas'
                },
                {
                    id: 4,
                    parentId: 3, 
                    label: 'Descargue Reviews App'
                },
                {
                    id: 5,
                    parentId: 3, 
                    label: 'Ajustes de Reputación'
                }
            ]
        },
        {
            id: 4,
            label: 'Aborde a sus clientes',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/engage.svg',
            link: ' ',
            children: [
                {
                    id: 1,
                    parentId: 4, 
                    label: 'Directorio de clientes'
                },
                {
                    id: 2,
                    parentId: 4,
                    label: 'Ofertas'
                },
                {
                    id: 3,
                    parentId: 4,
                    label: 'Configuración de correo electrónico del cliente'
                },
                {
                    id: 4,
                    parentId: 4,
                    label: 'Resultados de correo electrónico del cliente'
                },
                {
                    id: 5,
                    parentId: 4,
                    label: 'Administrar comentarios de los clientes'
                } 
            ]
        }
    ]
}

export const AlternateMenuResource = {
    en: [
        {
            id: 1,
            label: 'Settings',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/settings.svg',
            children: []
        },
        {
            id: 2,
            label: 'Get Help',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/help.svg',
            children: []
        },
        {
            id: 3,
            label: 'Sign out',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/sign-out.svg',
            children: []
        },
    ],

    es: [
        {
            id: 1,
            label: 'Ajustes',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/settings.svg',
            children: []
        },
        {
            id: 2,
            label: 'Obtenga Ayuda',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/help.svg',
            children: []
        },
        {
            id: 3,
            label: 'Salir',
            thumbnail: 'https://app00.testing.womply.com/dashboard/assets/icons/sign-out.svg',
            children: []
        },
    ]
}