import React from 'react';

import { Button } from '@storybook/react/demo'

import { Logo } from '../components/Brand/';
import { SideNav } from '../components/SideNav';

export default {
    title: 'Brand',
    component: Button
}

export const AppLogo = () => <SideNav><Logo/></SideNav>
